# language: pt
@login
Funcionalidade: Login no Ecommerce Fake
  Como um cliente do site FakeEcommerce
  Gostaria de poder me logar 
  Para poder realizar compras

  # Completar o cenário abaixo
  Cenário: Realizar login
  Dado que esteja na tela de login 
  Quando submeter as credenciais "sarahagnys18@gmail.com" e "senha123"
  Entao deverá logar com sucesso

  # Completar o cenário abaixo
  Cenário: Login com falha
  Dado que esteja na tela de login 
  Quando submeter as credenciais "sarahagnys18@gmail.com" e "senha"
  Entao não deverá logar com sucesso