# language: pt
@adicionar
Funcionalidade: Adicionar item ao carrinho
  Como um cliente do site FakeEcommerce
  Gostaria de adicionar compras ao carrinho

  Contexto:
    Dado que esteja na página inicial

Cenario: Adicionar item 
 Quando selecionar o primeiro item da tela 
 Entao devera adicionar ao carrinho com sucesso 
