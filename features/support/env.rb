require 'capybara/cucumber'
require 'selenium-webdriver'    
require 'site_prism'

Capybara.configure do |config|
    Selenium::WebDriver::Chrome::Service.driver_path =  'C:\Tools\webdrivers\geckodriver.exe' #'C:\Tools\webdrivers\chromedriver.exe' 
    config.default_driver = :selenium #:selenium_chrome
    config.app_host = 'https://automacaocombatista.herokuapp.com'    
    config.default_max_wait_time = 5
    end
