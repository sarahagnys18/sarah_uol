Dado('que esteja na tela de login') do
    @login_page.load
  end
  
  Quando('submeter as credenciais {string} e {string}') do |email, senha|
    @login_page.logar(email,senha)
  end
  
  Entao('deverá logar com sucesso') do
    expect(page).to have_content('Welcome to your account. Here you can manage all of your personal information and orders.')
  end

  Entao('não deverá logar com sucesso') do
    expect(@login_page.alerta).to eql "There is 1 error\nAuthentication failed."
  end
  
  