Quando('selecionar o primeiro item da tela') do                                
    @add_page.selecionar_produto
  end                                                                            
                                                                                 
  Entao('devera adicionar ao carrinho com sucesso') do                           
    expect(page).to have_content "Proceed to checkout"
  end                                                                            